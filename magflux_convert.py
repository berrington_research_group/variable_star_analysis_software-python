# -*- coding: utf-8 -*-
"""
Created on Sat Jul 11 21:41:45 2020
@author: Alec Neal

Simple program to convert a file with HJD, mag/flux, and errors
to either magnitudes or fluxes. Upon converting, there's
an option to normalize the converted values: if you converted
to flux, each flux and flux error will be divided by the maximum
flux value. The magnitdues are normalized by the magnitude 
with the most intensity (smallest value). The mag errors are unchanged,
however, because magerr = ~1.086/SNR---it is unaffected by scaling.
"""

import pandas as pd
import numpy as np

input_file=input('File name: ')

df=pd.read_csv(input_file,header=None,sep=None,engine='python')
HJD=df[0] ; values=df[1] ; errors=df[2]

def convert_choice():
    print('Conversion:\n1. Magnitdues to fluxes\n2. Fluxes to magnitdues')
    choice=input('Choose a conversion: ')
    if choice == '1' or choice == '2':
        return choice
    else:
        print('\nPlease choose options 1 or 2')
        return convert_choice()
        
conversion=convert_choice()

if conversion == '1':
    converted_values=10**(-0.4*values)
    converted_errors=0.4*np.log(10)*errors*converted_values
    convert_str='flux'
elif conversion == '2':
    converted_values=-2.5*np.log10(values)
    converted_errors=(2.5*errors)/(np.log(10)*values)
    convert_str='mag'

print("""Would you like the data normalized?\n1. No\n2. Yes""")
normalize=input('')

if normalize == '2':
    if conversion == '1':
        max_flux=max(converted_values)
        final_values=converted_values/max_flux
        final_errors=converted_errors/max_flux
        convert_str='norm-'+convert_str
    elif conversion == '2':
        max_mag=min(converted_values)
        final_values=converted_values-max_mag
        final_errors=converted_errors
        # magnitude errors are unaffected by scaling
else:
    final_values=converted_values
    final_errors=converted_errors

for n in range(len(HJD)):
    HJD[n]=round(HJD[n],8)

pd.options.display.float_format = '{:.6f}'.format

df[1]=final_values ; df[2]=final_errors

pd.set_option('display.max_rows', None, 'display.max_columns', None)

output_file=convert_str+'_'+input_file
df.to_csv(output_file,index=False,sep='\t',header=None)
print(output_file+' saved.')
